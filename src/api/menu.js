import axiosUtil from "../config/utils/axiosUtil";

/** 根据父节点查询菜单 **/
export function loadMenu(pid){
    return axiosUtil.get('/sys/menu/load/'+pid);
}

/** 保存菜单数据 **/
export function save(data){
    return axiosUtil.post('/sys/menu/save', data);
}

export function deleteById(id){
    return axiosUtil.del("/sys/menu/"+id);
}
