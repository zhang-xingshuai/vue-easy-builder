import axiosUtil from "../config/utils/axiosUtil";

/** 根据父节点查询菜单 **/
export function page(param){
    return axiosUtil.post('/sys/role/page',param);
}

/** 保存菜单数据 **/
export function save(data){
    return axiosUtil.post('/sys/role/save', data);
}

export function deleteById(id){
    return axiosUtil.del("/sys/role/"+id);
}
