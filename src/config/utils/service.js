import Vue from 'vue';
import {Message}  from 'element-ui';

var service = {

    post: async function(url, param) {
        console.log("请求地址：" + url + " 请求参数：" + param);
        Vue.prototype.$axios.post(url, param).then((res) => {

            //console.log("请求成功：" + JSON.stringify(res));
            console.log(res);
            if(res != null){
                if( res.status == 500 || res.data.status == 500) {
                    Message.error('服务器繁忙，请稍后再试！');
                }
                if( res.status == 400 || res.data.status == 400) {
                    Message.error(res.message);
                }
                if( res.status == 200) {
                    return res.data.data;
                }

            }
        }).catch(e => {
            console.log(e);
            Message.warning('服务器繁忙，请稍后再试！');
        });
    },

    get:async function(url, param) {
        console.log("请求地址：" + url + " 请求参数：" + param);
        var data = await Vue.prototype.$axios.get(url, param).then((res) => {
            console.log("请求成功：" + JSON.stringify(res) );
            if(res != null){
                if( res.status == 500 || res.data.status == 500) {
                    
                    Message.error('服务器繁忙，请稍后再试！');
                }
                if( res.status == 400 || res.data.status == 400) {
                    Message.error(res.message);
                }
                if( res.status == 200) {
                    return res.data.data;
                }

            }
        }).catch(e => {
            console.log(e);
            Message.warning('服务器繁忙，请稍后再试！');
        });
        
        return  data;
    }
    
}

export default service