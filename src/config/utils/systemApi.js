import service from "./service";

export const loadRouter = async ()=> {
    console.log("调用加载路由方法。。。。。");
    let routes = [
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/login',
            name: 'login',
            component: ()=> import('@/view/login/index.vue')
        },
        {
            path: '/index',
            name: 'index',
            component: ()=> import('@/view/index.vue'),
            children: [
                {
                    path: '/start',
                    name: 'start',
                    component: () => import('@/view/system/Start.vue')
                },
                {
                    path: '/system/TableManage',
                    name: 'TableManage',
                    component: () => import('@/view/system/TableManage.vue')
                },
                {
                    path: '/system/FunctionManage',
                    name: 'FunctionManage',
                    component: () => import('@/view/system/FunctionManage.vue')
                },
                {
                    path: '/system/MenuManage',
                    name: 'MenuManage',
                    component: () => import('@/view/system/MenuManage.vue')
                },
                {
                    path: '/organization/UserManage',
                    name: 'UserManage',
                    component: () => import('@/view/organization/user/index.vue')
                },
                {
                    path: '/organization/RoleManage',
                    name: 'RoleManage',
                    component: () => import('@/view/organization/role/index.vue')
                },
                {
                    path: '/organization/DeptManage',
                    name: 'DeptManage',
                    component: () => import('@/view/organization/dept/index.vue')
                },
                {
                    name: '404',
                    path: '/404',
                    component: () => import('@/view/error/404.vue')
                },
            ]
        },
        {
            path: '*',    // 此处需特别注意至于最底部
            redirect: '/404'
        }
    ];
    return routes;
}
