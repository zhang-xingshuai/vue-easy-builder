
let cache = {

    // 设置缓存
    setCache: function(key, value) {
        localStorage.setItem(key, value);
    },

    // 加载缓存
    loadCache: async function(key) {
        return localStorage.getItem(key);
    },

    // 清除缓存
    clearCache: async function(key) {
        localStorage.removeItem(key);
    },

    // 清除所有缓存
    clearAllCache: function() {
        localStorage.clear();
    }

}


export default cache