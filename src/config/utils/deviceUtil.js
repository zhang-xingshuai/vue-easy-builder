// 设备工具类

var deviceUtil = {
  // 判断是否为手机设备
  isPhone: function() {
    let flag = navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i)
    if(flag != null){
      console.log("移动设备");
      return true;
    }
    console.log("pc设备");
    return false;
  },
  // 手机震动
  vibrate: function(time) {
    console.log("js 震动");
    navigator.vibrate = navigator.vibrate
               || navigator.webkitVibrate
               || navigator.mozVibrate
               || navigator.msVibrate;

    if(navigator.vibrate) {
      // window.navigator.vibrate(100); 震动
      navigator.vibrate(time); // 100：表示震动100ms；[200, 100, 200]：震动200停100再震动200，和qq的消息震动一样
    }else {
      console.log("浏览器不支持震动")
    }
  },
  
  // 获取设备高度
  getWindowHeight: function(){
    
  },

  isSafariBrowser: function(){
    let isSafariBrowser = /Safari/.test(navigator.userAgent) && !/Chrome/.test(navigator.userAgent);
    return isSafariBrowser;
  }
}

export default deviceUtil
