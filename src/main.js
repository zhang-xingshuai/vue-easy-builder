import createApp from 'vue'
import Vue from 'vue';
import App from './App.vue';
import ElementUI from 'element-ui'; // 引入vv
import 'element-ui/lib/theme-chalk/index.css';
import 'ant-design-vue/dist/antd.css';
import router from './router/index';
import axios from 'axios';
import Fragment from 'vue-fragment'; // 为了解决左侧菜单收起后，文字不隐藏而引入



Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.use(Fragment.Plugin);
Vue.prototype.$axios = axios;
axios.defaults.baseURL = '/api';

// 定义启动方法
const start = async function (){
  // 加载路由数据
  let routers = await router();

  const app = new Vue({
    router: routers,
    render: h=>h(App)
  }).$mount('#app');

  console.log("app创建完成");
}

// 项目启动
start();
