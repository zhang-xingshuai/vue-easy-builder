import Vue from 'vue';
import VueRouter from 'vue-router';
import {loadRouter} from '../config/utils/systemApi';
Vue.use(VueRouter);

export async function initRouter() {

    let routers = await loadRouter();

    return new VueRouter({
        routes: routers
    });
}

const router = ()=>initRouter();

function isPhone(){
    let phones = ["iPhone", "Android"];
    let userAgent = navigator.userAgent;
    console.log("userAgent:" + userAgent);
    for (let i=0; i< phones.length; i++) {
        if (userAgent.indexOf(phones[i]) > 0) {
            console.log("手机设备");
            return true;
        }
    }
    console.log("PC设备");
    return false;
}
// router.beforeEach((to, from, next) => {
//     if (to.meta.isAuth) { // 判定是否需要授权
//         if (localStorage.getItem('name') === 'llx') {
//             next();

//         } else {
//             alert('姓名不对');
//         }

//     } else {
//         next();

//     }
// });

// router.afterEach((to, from, next) => {  // eslint-disable-line no-unused-vars
//     //document.title = to.name;
// })

export default router;